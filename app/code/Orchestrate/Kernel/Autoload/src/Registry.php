<?php

namespace Orchestrate\Kernel\Autoload;

/**
 * Registry to store a static member autoloader.
 *
 */
class Registry
{
    /**
     * @var AutoloaderInterface
     */
    protected static $autoloader;

    /**
     * Registers the given autoloader as a static member
     *
     * @param AutoloaderInterface $autoloader
     * @return void
     */
    public static function registerAutoloader(AutoloaderInterface $autoloader)
    {
        self::$autoloader = $autoloader;
    }

    /**
     * Returns the registered autoloader
     *
     * @throws \Exception
     * @return AutoloaderInterface
     */
    public static function getAutoloader()
    {
        if (self::$autoloader !== null) {
            return self::$autoloader;
        } else {
            throw new \Exception('Autoloader is not registered, cannot be retrieved.');
        }
    }
}
