<?php

namespace Orchestrate\Kernel\Component;

/**
 * Registry interface
 *
 */
interface RegistryInterface
{
    /**
     * Get list of all registered components. Returns an array where key is fully-qualified component name and value is
     * absolute path to component.
     *
     * @param string $type
     * @return array
     */
    public function getPaths($type);

    /**
     * Return the path of the component if it is already registered.
     *
     * @param string $type
     * @param string $componentName
     * @return null|string
     */
    public function getPath($type, $componentName);
}
