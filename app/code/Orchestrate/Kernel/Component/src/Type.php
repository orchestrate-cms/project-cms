<?php

namespace Orchestrate\Kernel\Component;

/**
 * Registry class which provides the ability to statically register components.
 *
 */
class Type
{
    /**
     * Language component type constant.
     *
     * @var string
     */
    const LANGUAGE = 'language';

    /**
     * Library component type constant.
     *
     * @var string
     */
    const LIBRARY = 'library';

    /**
     * Module component type constant.
     *
     * @var string
     */
    const MODULE = 'module';

    /**
     * Type component type constant.
     *
     * @var string
     */
    const THEME = 'theme';
}