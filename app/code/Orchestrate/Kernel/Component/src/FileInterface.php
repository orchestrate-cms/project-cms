<?php

namespace Orchestrate\Kernel\Component;

/**
 * File information interface holding meta data for files found in components.
 *
 */
interface FileInterface
{
    /**
     * Get component type
     *
     * @return string
     */
    public function getComponentType();

    /**
     * Get component name
     *
     * @return string
     */
    public function getComponentName();

    /**
     * Get full path to the component
     *
     * @return string
     */
    public function getFullPath();
}
