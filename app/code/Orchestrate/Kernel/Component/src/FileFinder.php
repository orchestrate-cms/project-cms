<?php

namespace Orchestrate\Kernel\Component;

use Orchestrate\Kernel\Io\Directory\ReaderFactory;

/**
 * Class for searching files across all locations of certain component type.
 *
 */
class FileFinder
{
    /**
     * Component registry
     *
     * @var RegistryInterface
     */
    private $registry;

    /**
     * Directory reader factory
     *
     * @var ReaderFactory
     */
    private $directoryReaderFactory;

    /**
     * File factory
     *
     * @var FileFactory
     */
    private $fileFactory;

    /**
     * Constructor
     *
     * @param RegistryInterface $registry
     * @param ReaderFactory $directoryReaderFactory
     * @param FileFactory $fileFactory
     */
    public function __construct(
        RegistryInterface $registry,
        ReaderFactory $directoryReaderFactory,
        FileFactory $fileFactory
    ) {
        $this->registry = $registry;
        $this->directoryReaderFactory = $directoryReaderFactory;
        $this->fileFactory = $fileFactory;
    }

    /**
     * Search for files in each component by pattern, returns absolute paths
     *
     * @param string $componentType
     * @param string $pattern
     * @return array
     */
    public function collectFiles($componentType, $pattern)
    {
        return $this->collect($componentType, $pattern, false);
    }

    /**
     * Search for files in each component by pattern, returns file objects with absolute file paths
     *
     * @param string $componentType
     * @param string $pattern
     * @return File[]
     */
    public function collectFilesWithContext($componentType, $pattern)
    {
        return $this->collect($componentType, $pattern, true);
    }

    /**
     * Collect files in components
     * If $withContext is true, returns array of file objects with component context
     *
     * @param string $componentType
     * @param string $pattern
     * @param bool|false $withContext
     * @return array
     */
    private function collect($componentType, $pattern, $withContext)
    {
        $files = [];
        foreach ($this->registry->getPaths($componentType) as $componentName => $path) {
            $directoryRead = $this->directoryReaderFactory->create($path);
            $foundFiles = $directoryRead->search($pattern);
            foreach ($foundFiles as $foundFile) {
                $foundFile = $directoryRead->getAbsolutePath($foundFile);
                if ($withContext) {
                    $files[] = $this->fileFactory->create($componentType, $componentName, $foundFile);
                } else {
                    $files[] = $foundFile;
                }
            }
        }
        return $files;
    }
}
