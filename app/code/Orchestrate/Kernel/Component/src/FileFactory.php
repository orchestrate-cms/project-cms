<?php

namespace Orchestrate\Kernel\Component;

use \Orchestrate\Kernel\Di\ObjectManagerInterface;

/**
 * File factory class
 *
 */
class FileFactory
{
    /**
     * Object Manager instance
     *
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * Name of instance to create
     *
     * @var string
     */
    protected $instanceName;

    /**
     * Constructor
     *
     * @param ObjectManagerInterface $objectManager
     * @param string $instanceName
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        $instanceName = FileInterface::class
    ) {
        $this->objectManager = $objectManager;
        $this->instanceName = $instanceName;
    }

    /**
     * Create info instance
     *
     * @param string $componentType
     * @param string $componentName
     * @param string $fullPath
     * @return FileInterface
     */
    public function create($componentType, $componentName, $fullPath)
    {
        return $this->objectManager->create(
            $this->instanceName,
            ['componentType' => $componentType, 'componentName' => $componentName, 'fullPath' => $fullPath]
        );
    }
}
